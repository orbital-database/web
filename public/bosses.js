var LANG;
fetch('base.json').then(resp => resp.text()).then(data => {
    LANG = JSON.parse(data);
});

var BOSSES;
fetch('data/bosses.json').then(resp => resp.text()).then(data => {
    BOSSES = JSON.parse(data);
    loadBossList();
    initExpandables();
    openMenu();
})

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
});

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openBoss(key) {
    obj = document.getElementById(key);
    var table = document.getElementById("list");
    var img = document.getElementById("image");
    table.innerHTML = "";
    img.src = "";
    
    var boss_text = document.querySelectorAll(".boss-text");
    for (let i = 0; i < boss_text.length; i++) {
        boss_text[i].classList.add("white-text");
        boss_text[i].classList.remove("white");
    }
    
    obj.classList.remove("white-text");
    obj.classList.add("white");

    var path = document.getElementById("path");
    addData(BOSSES[key]);
    path.innerHTML = `> ${BOSSES[key].name.toUpperCase()}`;
    
    var instances = M.AutoInit();
    initExpandables();
}

function addData(json) {
    var items = document.getElementById("items");
    
    var img = document.getElementById("image");
    img.src = `../img/bosses/${json.id}.png`;
    
    var table = document.getElementById("list");
    
    var col_ul = document.createElement("ul");
    col_ul.className = "collection with-header";
    
    var header = document.createElement("li");
    header.className = "collection-header white-text";
    header.innerHTML = `<h5>${json.name}</h5>`;
    col_ul.appendChild(header);
    
    hdr0 = document.createElement("li");
    hdr0.className = "collection-item white-text";
    hdr0.innerHTML = `<b>${LANG.sta_l}:</b>`;
    col_ul.appendChild(hdr0);
    
    stats = document.createElement("li");
    stats.className = "collection-item white-text";
    
    if (json.stats_alt) {
        stats.innerHTML = `${LANG.vp}: ${json.stats.vp}<br>
                        ${LANG.def}: ${json.stats.defence} (${json.stats_alt.defence})<br>
                        ${LANG.defp}: ${json.stats.defence_penalty} (${json.stats_alt.defence_penalty})<br>
                        ${LANG.bdef}: ${json.stats.bullet_def} (${json.stats_alt.bullet_def})<br>
                        ${LANG.ldef}: ${json.stats.laser_def} (${json.stats_alt.laser_def})<br><hr>
                        ${LANG.bres}: ${json.stats.burn_res} (${json.stats_alt.burn_res})<br>
                        ${LANG.burnsd}: ${json.stats.burn_speed_down} (${json.stats_alt.burn_speed_down})<br>
                        ${LANG.sres}: ${json.stats.stun_res} (${json.stats_alt.stun_res})<br>
                        ${LANG.stunsd}: ${json.stats.stun_speed_down} (${json.stats_alt.stun_speed_down})<br>
                        ${LANG.acidr}: ${json.stats.acid_res} (${json.stats_alt.acid_res})<br>
                        ${LANG.acidsd}: ${json.stats.acid_speed_down} (${json.stats_alt.acid_speed_down})<br>
                        ${LANG.fres}: ${json.stats.flash_res} (${json.stats_alt.flash_res})<br>
                        ${LANG.jres}: ${json.stats.jam_res} (${json.stats_alt.jam_res})<br>`;
    } else {
        stats.innerHTML = `${LANG.vp}: ${json.stats.vp}<br>
                    ${LANG.def}: ${json.stats.defence}<br>
                    ${LANG.defp}: ${json.stats.defence_penalty}<br>
                    ${LANG.bdef}: ${json.stats.bullet_def}<br>
                    ${LANG.ldef}: ${json.stats.laser_def}<br><hr>
                    ${LANG.bres}: ${json.stats.burn_res}<br>
                    ${LANG.burnsd}: ${json.stats.burn_speed_down}<br>
                    ${LANG.sres}: ${json.stats.stun_res}<br>
                    ${LANG.stunsd}: ${json.stats.stun_speed_down}<br>
                    ${LANG.acidr}: ${json.stats.acid_res}<br>
                    ${LANG.acidsd}: ${json.stats.acid_speed_down}<br>
                    ${LANG.fres}: ${json.stats.flash_res}<br>
                    ${LANG.jres}: ${json.stats.jam_res}<br>`;
    }

    col_ul.appendChild(stats);
    
    hdr1 = document.createElement("li");
    hdr1.className = "collection-item white-text";
    hdr1.innerHTML = `<b>${LANG.miss_l}:</b>`;
    col_ul.appendChild(hdr1);
    
    var itm = document.createElement("li");
    itm.className = "collection-item white-text";
    var missions = "";
    for (let i = 0; i < json.missions.length; i++) {
        var text = `${json.missions[i].title}<br>
                    ${json.missions[i].subtitle} (${json.missions[i].category.toUpperCase().replace("_", " ")})
                    ${i == json.missions.length-1 ? "" : "<br><hr>"}`

        missions = missions+text;
    }
    itm.innerHTML = missions;
    col_ul.appendChild(itm);

    hdr2 = document.createElement("li");
    hdr2.className = "collection-item white-text";
    hdr2.innerHTML = `<b>${LANG.drop}:</b>`;
    col_ul.appendChild(hdr2);
    
    var drops = document.createElement("li");
    drops.className = "collection-item white-text";
    var dtext = "";
    
    if (json.base_drops.length != 0) {
        for (let i = 0; i < json.base_drops.length; i++) {
            var bdrops = "";
            for (let j = 0; j < json.base_drops[i].attachments.length; j++) {
                bdrops = bdrops+`- ${json.base_drops[i].attachments[j].name} (${json.base_drops[i].attachments[j].rate}%)<br>`;
            }
            
            dtext = dtext+`${json.base_drops[i].text}:<br>${bdrops}<hr>`;
        }
    }

    if (json.weakpoint_drops.length != 0) {
        for (let i = 0; i < json.weakpoint_drops.length; i++) {
            var wdrops = "";
            for (let j = 0; j < json.weakpoint_drops[i].attachments.length; j++) {
                wdrops = wdrops+`- ${json.weakpoint_drops[i].attachments[j].name} (${json.weakpoint_drops[i].attachments[j].rate}%)<br>`;
            }
            
            dtext = dtext+`${json.weakpoint_drops[i].text} (${json.weakpoint_drops[i].count}):<br>${wdrops}<hr>`;
        }
    }
    
    if (json.unique_drops.length != 0) {
        for (let i = 0; i < json.unique_drops.length; i++) {
            var udrops = "";
            for (let j = 0; j < json.unique_drops[i].drops.attachments.length; j++) {
                udrops = udrops+`- ${json.unique_drops[i].drops.attachments[j].name} (${json.unique_drops[i].drops.attachments[j].rate}%)<br>`;
            }

            dtext = dtext+`${json.unique_drops[i].drops.text} (${json.unique_drops[i].value}):<br>${udrops}<hr>`;
        }
    }
    
    drops.innerHTML = dtext.slice(0, -4);
    col_ul.appendChild(drops);
    
    if (json.blueprints.length != 0) {
        var itm1 = document.createElement("li");
        itm1.className = "collection-item white-text";
        var bluep = "";
        for (let i = 0; i < json.blueprints.length; i++) {
            bluep = bluep+`- ${json.blueprints[i].name} (${json.blueprints[i].rate}%)<br>`;
        }
        itm1.innerHTML = `<b>${LANG.bluep}:</b><br>${bluep}`;
        col_ul.appendChild(itm1);
    }

    table.appendChild(col_ul);
}

function loadBossList() {
    var list = document.getElementById("boss-list");
    
    Object.keys(BOSSES).forEach(function(key,index) {
        var gli = document.createElement("li");
        gli.className = "boss";
        
        var ndiv = document.createElement("div");
        ndiv.className = "boss-text white-text";
        ndiv.id = key;
        ndiv.innerHTML = BOSSES[key].name;
        
        ndiv.addEventListener("click", function(e) {
            openBoss(key);
        }, false);
        
        gli.appendChild(ndiv);
        
        list.appendChild(gli);
    });
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}
