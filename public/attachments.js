var LANG;
fetch('base.json').then(resp => resp.text()).then(data => {
    LANG = JSON.parse(data);
});

var ATTACHMENTS;
fetch('data/attachments.json').then(resp => resp.text()).then(data => {
    ATTACHMENTS = JSON.parse(data);
    initExpandables();
    openMenu();
})

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
});

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openCategory(obj) {
    var rank_text = document.querySelectorAll(".category-text");
    for (let i = 0; i < rank_text.length; i++) {
        rank_text[i].classList.add("white-text");
        rank_text[i].classList.remove("white");
    }
    
    if (obj.id == "common") {
        obj.classList.remove("white-text");
        obj.setAttribute("style", "background-color: #fff !important;");
    } else {
        var coop = document.getElementById("common");
        coop.setAttribute("style", "background-color: #fff;");
        coop.classList.add("white-text");

        obj.classList.remove("white-text");
        obj.classList.add("white");
    }
    
    switch(obj.id) {
    case 'common':
        addAttachments(ATTACHMENTS.common);
        path.innerHTML = `> ${LANG.comm}`;
        break;
    case 'acommon':
        addAttachments(ATTACHMENTS.parts.common);
        path.innerHTML = `> ${LANG.part} > ${LANG.comm}`;
        break;
    case 'head':
        addAttachments(ATTACHMENTS.parts.head);
        path.innerHTML = `> ${LANG.part} > ${LANG.head_u}`;
        break;
    case 'body':
        addAttachments(ATTACHMENTS.parts.body);
        path.innerHTML = `> ${LANG.part} > ${LANG.body_u}`;
        break;
    case 'arms':
        addAttachments(ATTACHMENTS.parts.arms);
        path.innerHTML = `> ${LANG.part} > ${LANG.arms}`;
        break;
    case 'legs':
        addAttachments(ATTACHMENTS.parts.legs);
        path.innerHTML = `> ${LANG.part} > ${LANG.leg_u}`;
        break;
    case 'wcommon':
        addAttachments(ATTACHMENTS.weapons.common);
        path.innerHTML = `> ${LANG.wep} > ${LANG.comm}`;
        break;
    case 'bullet':
        addAttachments(ATTACHMENTS.weapons.bullet);
        path.innerHTML = `> ${LANG.wep} > ${LANG.bullet}`;
        break;
    case 'laser':
        addAttachments(ATTACHMENTS.weapons.laser);
        path.innerHTML = `> ${LANG.wep} > ${LANG.laser}`;
        break;
    case 'melee':
        addAttachments(ATTACHMENTS.weapons.melee);
        path.innerHTML = `> ${LANG.wep} > ${LANG.melee}`;
        break;
    case 'special':
        addAttachments(ATTACHMENTS.weapons.special);
        path.innerHTML = `> ${LANG.wep} > ${LANG.sp}`;
        break;
    case 'shield':
        addAttachments(ATTACHMENTS.weapons.shield);
        path.innerHTML = `> ${LANG.wep} > ${LANG.shld}`;
        break;
    case 'missiles':
        addAttachments(ATTACHMENTS.weapons.missiles);
        path.innerHTML = `> ${LANG.wep} > ${LANG.missi}`;
        break;
    case 'blitz':
        addAttachments(ATTACHMENTS.weapons.blitz);
        path.innerHTML = `> ${LANG.wep} > ${LANG.blitz}`;
        break;
    case 'support':
        addAttachments(ATTACHMENTS.weapons.support);
        path.innerHTML = `> ${LANG.wep} > ${LANG.sup}`;
        break;
        
    }

    var instances = M.AutoInit();
    initExpandables();
}

function addAttachments(json) {
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    for (let i = 0; i < json.length; i++) {
        var col_ul = document.createElement("ul");
        col_ul.className = "collection";
        
        var col_itm_li = document.createElement("li");
        col_itm_li.className = "collection-item white-text";
        col_ul.appendChild(col_itm_li);
        
        var col_title = document.createElement("span");
        col_title.className = "title";
        col_title.innerHTML = `<b>${json[i].name}</b>`;
        col_itm_li.appendChild(col_title);
        
        var desc = document.createElement("p");
        desc.className = "line";
        desc.innerHTML = `<hr>${json[i].description}`;
        col_itm_li.appendChild(desc);
        
        var prices = document.createElement("p");
        prices.className = "line";
        prices.innerHTML = `<hr>${LANG.sellp}: ${json[i].sale_price}c<br>${LANG.equipc}: ${json[i].equip_cost}c`;
        col_itm_li.appendChild(prices);
        
        var stats = document.createElement("p");
        stats.className = "line";
        stats.innerHTML = `<hr><b>${LANG.sta_u}:</b><br>${listStats(json[i].stats)}`;
        col_itm_li.appendChild(stats);
        
        table.appendChild(col_ul);
    }
}

function listStats(array) {
    var res = "";
    for (let i = 0; i < array.length; i++) {
        var name = array[i].name;
        var value = array[i].value;
        var perc = array[i].percentage;
        
        if (value == undefined) {
            var num = perc < 0 ? `${(perc*100).toFixed(2)}%`: `+${(perc*100).toFixed(2)}%`;
        } else {
            var num = value < 0 ? value : "+"+value;
        }
        
        var text = `- ${name}: ${num}`;
        res = res + `${text}<br>`;
    }
    
    return res;
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}
