var CACHE = new Set();

var LANG;
fetch('base.json').then(resp => resp.text()).then(data => {
    LANG = JSON.parse(data);
});

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
    
    initExpandables();
    openMenu();
});

function loadJson(id) {
    var file;
    var WEAPONS;
    
    switch(id.split(".")[0]) {
        case 'a':
            file = "arm"
            break;
        case 's':
            file = "shoulder"
            break;
        case 'x':
            file = "aux"
            break;
    }
    
    for (let item of CACHE) {
        if (file == Object.keys(item)[0]) {
            WEAPONS = item;
            loadData(WEAPONS, id);
            break;
        }
    }
    
    if (!WEAPONS) {
        fetch('data/weapons/'+file+'.json').then(resp => resp.text()).then(data => {
            WEAPONS = JSON.parse(data);
            loadData(WEAPONS, id);
            CACHE.add(WEAPONS);
        });
    }
}

function loadData(obj, id) {
    var path = document.getElementById("path");
    
    switch(id) {
    case 'a.bullet':
        addWeapons(obj.arm.bullet);
        path.innerHTML = `> ${LANG.warms} > ${LANG.bullet}`;
        break;
    case 'a.laser':
        addWeapons(obj.arm.laser);
        path.innerHTML = `> ${LANG.warms} > ${LANG.laser}`;
        break;
    case 'a.melee':
        addWeapons(obj.arm.melee);
        path.innerHTML = `> ${LANG.warms} > ${LANG.melee}`;
        break;
    case 'a.special':
        addWeapons(obj.arm.special);
        path.innerHTML = `> ${LANG.warms} > ${LANG.sp}`;
        break;
    case 'a.shield':
        addWeapons(obj.arm.shield);
        path.innerHTML = `> ${LANG.warms} > ${LANG.shld}`;
        break;
    case 's.missile':
        addWeapons(obj.shoulder.missile);
        path.innerHTML = `> ${LANG.shldr} > ${LANG.missi}`;
        break;
    case 's.cannon':
        addWeapons(obj.shoulder.cannon);
        path.innerHTML = `> ${LANG.shldr} > ${LANG.canon}`;
        break;
    case 's.railgun':
        addWeapons(obj.shoulder.railgun);
        path.innerHTML = `> ${LANG.shldr} > ${LANG.railg}`;
        break;
    case 's.blitz':
        addWeapons(obj.shoulder.blitz);
        path.innerHTML = `> ${LANG.shldr} > ${LANG.blitz}`;
        break;
    case 's.support':
        addWeapons(obj.shoulder.support);
        path.innerHTML = `> ${LANG.shldr} > ${LANG.sup}`;
        break;
    case 'x.grenade':
        addWeapons(obj.aux.grenade);
        path.innerHTML = `> ${LANG.aux} > ${LANG.gren}`;
        break;
    case 'x.mine':
        addWeapons(obj.aux.mine);
        path.innerHTML = `> ${LANG.aux} > ${LANG.mine}`;
        break;
    case 'x.support':
        addWeapons(obj.aux.support);
        path.innerHTML = `> ${LANG.aux} > ${LANG.sup}`;
        break;
    }

    var instances = M.AutoInit();
    initExpandables();
}

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openCategory(obj) {
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    var rank_text = document.querySelectorAll(".category-text");
    for (let i = 0; i < rank_text.length; i++) {
        rank_text[i].classList.add("white-text");
        rank_text[i].classList.remove("white");
    }

    obj.classList.remove("white-text");
    obj.classList.add("white");
    
    loadJson(obj.id);
}

function addWeapons(json) {
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    for (let i = 0; i < json.length; i++) {
        var col_ul = document.createElement("ul");
        col_ul.className = "collection";
        
        var col_itm_li = document.createElement("li");
        col_itm_li.className = "collection-item white-text";
        col_ul.appendChild(col_itm_li);
        
        var div_img = document.createElement("div");
        div_img.className = "imagebox";
        
        var imgp = `../img/weapons/${json[i].id}.png`;
        var filters = ["___t1", "___t2", "___t3", "_t1", "_t2", "_t3"];
        
        for (let f = 0; f < filters.length; f++) {
            imgp = imgp.replace(filters[f], "");
        }

        var col_img = document.createElement("img");
        col_img.className = "image";
        col_img.src = imgp;
        div_img.appendChild(col_img);
        
        var col_title = document.createElement("span");
        col_title.className = "title";
        col_title.innerHTML = `<b>${json[i].name}</b>`;
        div_img.appendChild(col_title);
        
        var col_cat = document.createElement("span");
        col_cat.className = "line";
        col_cat.innerHTML = `<br>${json[i].category}`;
        div_img.appendChild(col_cat);
        
        var desc = document.createElement("p");
        desc.className = "line";
        desc.innerHTML = `<hr>${json[i].description}`;
        div_img.appendChild(desc);
        
        var prices = document.createElement("p");
        prices.className = "line";
        prices.innerHTML = `<hr>${LANG.manuf}: ${json[i].maker}<br>${LANG.dropr}: ${json[i].droprate}%`;
        div_img.appendChild(prices);
        
        col_itm_li.appendChild(div_img);
        
        // var features = document.createElement("p");
        // features.className = "line";
        // features.innerHTML = `<hr><b>${LANG.feat}:</b><br>${listFeatures(json[i].features)}`;
        // col_itm_li.appendChild(features);
        
        var stats = document.createElement("p");
        stats.className = "line";
        stats.innerHTML = `<b>${LANG.sta_u}:</b>`;
        col_itm_li.appendChild(stats);
        
        var stats_col = document.createElement("ul");
        stats_col.className = "collapsible expandable";
        
        var ingame_li = document.createElement("li");
        var ingame_header = document.createElement("div");
        ingame_header.className = "collapsible-header";
        ingame_header.innerHTML = `<b>${LANG.ing}:</b>`;
        ingame_li.appendChild(ingame_header);
        
        var ingame_content = document.createElement("div");
        ingame_content.className = "collapsible-body";
        ingame_content.innerHTML = listStats(json[i].stats.ingame);
        ingame_li.appendChild(ingame_content);
        
        stats_col.appendChild(ingame_li);
        
        var hidden_li = document.createElement("li");
        var hidden_header = document.createElement("div");
        hidden_header.className = "collapsible-header";
        hidden_header.innerHTML = `<b>${LANG.hidden}:</b>`;
        hidden_li.appendChild(hidden_header);
        
        var hidden_content = document.createElement("div");
        hidden_content.className = "collapsible-body";
        hidden_content.innerHTML = listStats(json[i].stats.hidden);
        hidden_li.appendChild(hidden_content);
        
        stats_col.appendChild(hidden_li);
        col_itm_li.appendChild(stats_col);
        

        if (json[i].attachments.missions.length > 0 || json[i].attachments.explore.length > 0) {
            var stats = document.createElement("p");
            stats.className = "line";
            stats.innerHTML = `<hr><b>${LANG.attd}:</b>`;
            col_itm_li.appendChild(stats);
            
            var attach_col = document.createElement("ul");
            attach_col.className = "collapsible expandable";

            if (json[i].attachments.missions.length > 0) {
                var mission_li = document.createElement("li");
                var mission_header = document.createElement("div");
                mission_header.className = "collapsible-header";
                mission_header.innerHTML = `<b>${LANG.inm}:</b>`;
                mission_li.appendChild(mission_header);
                
                var mission_content = document.createElement("div");
                mission_content.className = "collapsible-body";
                mission_content.innerHTML = listAttachments(json[i].attachments.missions);
                mission_li.appendChild(mission_content);
                
                attach_col.appendChild(mission_li);
            }
            
            if (json[i].attachments.explore.length > 0) {
                var explore_li = document.createElement("li");
                var explore_header = document.createElement("div");
                explore_header.className = "collapsible-header";
                explore_header.innerHTML = `<b>${LANG.inem}:</b>`;
                explore_li.appendChild(explore_header);
                
                var explore_content = document.createElement("div");
                explore_content.className = "collapsible-body";
                explore_content.innerHTML = listAttachments(json[i].attachments.explore);
                explore_li.appendChild(explore_content);
                
                attach_col.appendChild(explore_li);
                
            }
            
            col_itm_li.appendChild(attach_col);
        }
        
        table.appendChild(col_ul);
    }
}

function isEmpty(obj) { 
   for (var x in obj) { return false; }
   return true;
}

function listAttachments(array) {
    var res = "";
    for (let i = 0; i < array.length; i++) {
        var name = array[i].name;
        var rate = array[i].droprate.toFixed(2);
        
        res = res + `- ${name} (${rate}%)<br>`;
        
        if (!isEmpty(array[i].requirement)) {
            var text = `&emsp;* ${LANG.req}: ${array[i].requirement.title} - ${array[i].requirement.subtitle}
                       (${array[i].requirement.category.toUpperCase().replace("_", " ")})
                       <br>`;
            
            res = res + text;
        }
    }
    
    return res;
}

function listFeatures(array) {
    var res = "";
    for (let i = 0; i < array.length; i++) {
        var name = array[i];
        res = res + `- ${name}<br>`;
    }
    
    return res;
}

function listStats(array) {
    var res = "";
    var filters = ["CriticalEndDistance", "ChargeBonusScale"]
    for (let i = 0; i < array.length; i++) {
        if (!filters.includes(array[i].id)) {
            var name = array[i].name == undefined ? array[i].id : array[i].name;
            var value = array[i].value;
            var num = value === true ? LANG.y : calcStats(array[i], array);
            
            var text = `- ${name}: ${num}`;
            res = res + `${text}<br>`;
        }
    }
    
    return res;
}

function calcStats(obj, array) {
    switch(obj.id) {
        case 'DistanceIgnoreGravity':
            return obj.value/100;
        case 'Range':
            return obj.value/100;
        case 'SequenceRadius':
            return obj.value/100;
        case 'ExplodeRadius':
            return obj.value/100;
        case 'MaxExplodeDamageRadius':
            return obj.value/100;
        case 'AbnormalSpaceRadius':
            return obj.value/100;
        case 'FunnelBattleRange':
            return obj.value/100;
        case 'FirstAttackMaxMoveLength':
            return obj.value/100;
        case 'LockOnDistance':
            return obj.value/100;
        case 'ActiveHomingDistance':
            return obj.value/100;
        case 'CriticalStartDistance':
            var end = getObjById('CriticalEndDistance', array).value;
            var avg = (obj.value+end)/2;
            return `${(avg)/100}±${(avg-obj.value)/100}`
        case 'MaxChargeBonusScale':
            var cbs = getObjById('ChargeBonusScale', array).value;
            return 1+cbs+obj.value;
        case 'ShockAbsorptionRate':
            return ((1-obj.value)*100).toFixed(0);
        case 'NormalAdditionalBullet':
            return (1+obj.value).toFixed(2);
        case 'BazookaAdditionalBullet':
            return (1+obj.value).toFixed(2);
        case 'MissileAdditionalBullet':
            return (1+obj.value).toFixed(2);
        case 'RailGunAdditionalBullet':
            return (1+obj.value).toFixed(2);
        case 'ArkGunAdditionalBullet':
            return (1+obj.value).toFixed(2);
        case 'AcidGunAdditionalBullet':
            return (1+obj.value).toFixed(2);
        case 'FlameThrowerAdditionalBullet':
            return (1+obj.value).toFixed(2);
        default:
            return obj.value;
    }
}

function getObjById(id, array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i].id == id) {
            return array[i];
        }
    }
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}
