var LANG;
fetch('base.json').then(resp => resp.text()).then(data => {
    LANG = JSON.parse(data);
});

var COLLECTIONS;
fetch('data/collection.json').then(resp => resp.text()).then(data => {
    COLLECTIONS = JSON.parse(data);
    initExpandables();
    openMenu();
})

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
});

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openRank(obj) {
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    var rank_text = document.querySelectorAll(".rank-text");
    for (let i = 0; i < rank_text.length; i++) {
        rank_text[i].classList.add("white-text");
        rank_text[i].classList.remove("white");
    }
    
    obj.classList.remove("white-text");
    obj.classList.add("white");

    var path = document.getElementById("path");
    switch(obj.id) {
    case 'patterns':
        addCollection(COLLECTIONS.patterns);
        path.innerHTML = `> ${LANG.patt}`;
        break;
    case 'decals':
        addCollection(COLLECTIONS.decals);
        path.innerHTML = `> ${LANG.decal}`;
        break;
    case 'titles':
        addCollection(COLLECTIONS.titles);
        path.innerHTML = `> ${LANG.title}`;
        break;
    }
    
    var instances = M.AutoInit();
    initExpandables();
}

function addCollection(json) {
    var table = document.getElementById("list");
    
    for (let i = 0; i < json.length; i++) {
        var col_ul = document.createElement("ul");
        col_ul.className = "collection";
        
        var col_itm_li = document.createElement("li");
        col_itm_li.className = "collection-item white-text";
        col_ul.appendChild(col_itm_li);
        
        if (!json[i].id.includes("Title")) {
            var imgn = json[i].id;
            
            if (imgn.includes("Surface")) {
                var imgn = imgn.replace("_0", "_").replace("_0", "_").replace("_0", "_");
            }
            
            if (imgn.includes("Decal")) {
                var imgn = imgn.replace("_00", "_");
            }
            
            var col_img = document.createElement("img");
            col_img.className = "image";
            col_img.src = `../img/collection/${imgn}.png`;
            col_itm_li.appendChild(col_img);
        }
        
        var text_div = document.createElement("div");
        text_div.className = "text_div";
        
        var col_title = document.createElement("span");
        col_title.className = "title";
        col_title.innerHTML = `<b>${json[i].name}</b>`;
        text_div.appendChild(col_title);
        
        var col_line = document.createElement("p");
        col_line.className = "line";
        col_line.innerHTML = `${LANG.cond}: ${json[i].condition}`;
        text_div.appendChild(col_line);
        
        col_itm_li.appendChild(text_div);
        table.appendChild(col_ul);
    }
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}
