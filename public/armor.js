var CACHE = new Set();

var LANG;
fetch('base.json').then(resp => resp.text()).then(data => {
    LANG = JSON.parse(data);
});

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
    
    initExpandables();
    openMenu();
});

function loadJson(id) {
    var ARMOR;
    
    for (let item of CACHE) {
        if (id == Object.keys(item)[0]) {
            ARMOR = item;
            loadData(ARMOR, id);
            break;
        }
    }

    if (!ARMOR) {
        fetch('data/armor/'+id+'.json').then(resp => resp.text()).then(data => {
            ARMOR = JSON.parse(data);
            loadData(ARMOR, id);
            CACHE.add(ARMOR);
        });
    }
}

function loadData(obj, id) {
    var path = document.getElementById("path");
    
    switch(id) {
    case 'head':
        addArmor(obj.head);
        path.innerHTML = `> ${LANG.head_u}`;
        break;
    case 'body':
        addArmor(obj.body);
        path.innerHTML = `> ${LANG.body_u}`;
        break;
    case 'processor':
        addArmor(obj.processor);
        path.innerHTML = `> ${LANG.cpu_u}`;
        break;
    case 'left_arm':
        addArmor(obj.left_arm);
        path.innerHTML = `> ${LANG.larm_u}`;
        break;
    case 'right_arm':
        addArmor(obj.right_arm);
        path.innerHTML = `> ${LANG.rarm_u}`;
        break;
    case 'legs':
        addArmor(obj.legs);
        path.innerHTML = `> ${LANG.leg_u}`;
        break;
    }

    var instances = M.AutoInit();
    initExpandables();
}

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openCategory(obj) {
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    var rank_text = document.querySelectorAll(".rank-text");
    for (let i = 0; i < rank_text.length; i++) {
        rank_text[i].classList.add("white-text");
        rank_text[i].setAttribute("style", "background-color: #000000e6 !important;");
    }

    obj.classList.remove("white-text");
    obj.setAttribute("style", "background-color: #fff !important;");
    
    loadJson(obj.id);
}

function addArmor(json) {
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    for (let i = 0; i < json.length; i++) {
        var col_ul = document.createElement("ul");
        col_ul.className = "collection";
        
        var col_itm_li = document.createElement("li");
        col_itm_li.className = "collection-item white-text";
        col_ul.appendChild(col_itm_li);
        
        var div_img = document.createElement("div");
        div_img.className = "imagebox";
        
        var imgp;
        if (json[i].id.includes("AS_Inside")) {
            imgp = "../img/armor/AS_Inside_000.png";
        } else {
            imgp = `../img/armor/${json[i].id}.png`;
        }
        
        var col_img = document.createElement("img");
        col_img.className = "image";
        col_img.src = imgp;
        div_img.appendChild(col_img);
        
        var col_title = document.createElement("span");
        col_title.className = "title";
        col_title.innerHTML = `<b>${json[i].name}</b>`;
        div_img.appendChild(col_title);
        
        var desc = document.createElement("p");
        desc.className = "line";
        desc.innerHTML = `<hr>${json[i].description}`;
        div_img.appendChild(desc);
        
        var prices = document.createElement("p");
        prices.className = "line";
        prices.innerHTML = `<hr>${LANG.manuf}: ${json[i].maker}<br>${LANG.pric}: ${json[i].price}c<br><br>`;
        div_img.appendChild(prices);
        
        col_itm_li.appendChild(div_img);
        
        // var features = document.createElement("p");
        // features.className = "line";
        // features.innerHTML = `<hr><b>${LANG.feat}:</b><br>${listFeatures(json[i].features)}`;
        // col_itm_li.appendChild(features);
        
        var stats = document.createElement("p");
        stats.className = "line";
        stats.innerHTML = `<b>${LANG.sta_u}:</b>`;
        col_itm_li.appendChild(stats);
        
        var stats_col = document.createElement("ul");
        stats_col.className = "collapsible expandable";
        
        var ingame_li = document.createElement("li");
        var ingame_header = document.createElement("div");
        ingame_header.className = "collapsible-header";
        ingame_header.innerHTML = `<b>${LANG.ing}:</b>`;
        ingame_li.appendChild(ingame_header);
        
        var ingame_content = document.createElement("div");
        ingame_content.className = "collapsible-body";
        ingame_content.innerHTML = listStats(json[i].stats.ingame);
        ingame_li.appendChild(ingame_content);
        
        stats_col.appendChild(ingame_li);
        
        var hidden_li = document.createElement("li");
        var hidden_header = document.createElement("div");
        hidden_header.className = "collapsible-header";
        hidden_header.innerHTML = `<b>${LANG.hidden}:</b>`;
        hidden_li.appendChild(hidden_header);
        
        var hidden_content = document.createElement("div");
        hidden_content.className = "collapsible-body";
        hidden_content.innerHTML = listStats(json[i].stats.hidden);
        hidden_li.appendChild(hidden_content);
        
        stats_col.appendChild(hidden_li);
        col_itm_li.appendChild(stats_col);
        
        table.appendChild(col_ul);
    }
}

function isEmpty(obj) { 
   for (var x in obj) { return false; }
   return true;
}

function listAttachments(array) {
    var res = "";
    for (let i = 0; i < array.length; i++) {
        var name = array[i].name;
        var rate = array[i].droprate.toFixed(2);
        
        res = res + `- ${name} (${rate}%)<br>`;
        
        if (!isEmpty(array[i].requirement)) {
            var text = `&emsp;* ${LANG.req}: ${array[i].requirement.title} - ${array[i].requirement.subtitle}
                       (${array[i].requirement.category.toUpperCase().replace("_", " ")})
                       <br>`;
            
            res = res + text;
        }
    }
    
    return res;
}

function listFeatures(array) {
    if (array == undefined || array.length == 0) {
        return `- ${LANG.none2}`;
    }
    
    var res = "";
    for (let i = 0; i < array.length; i++) {
        var name = array[i];
        res = res + `- ${name}<br>`;
    }
    
    return res;
}

function listStats(array) {
    var res = "";
    var filters = ["LockonSightType", "LockonSightWidth"]
    for (let i = 0; i < array.length; i++) {
        if (!filters.includes(array[i].id)) {
            var name = array[i].name == undefined ? array[i].id : array[i].name;
            var value = array[i].value;
            var num = value === true ? LANG.y : calcStats(array[i], array);
            
            var text = `- ${name}: ${num}`;
            res = res + `${text}<br>`;
        }
    }
    
    return res;
}

function calcStats(obj, array) {
    switch(obj.id) {
        case 'LockonRange':
            return obj.value/100;
        case 'RadarRadius':
            return obj.value/100;
        case 'ShootCorrectSpec':
            return obj.value/100;
        case 'LockonSightHeight':
            var type = getObjById('LockonSightType', array).value;
            if (type == "LockonSight_Clrcle") {
                return `R ${obj.value}`;
            }
            if (type == "LockonSight_Square") {
                var width = getObjById('LockonSightWidth', array).value;
                return `S ${width}:${obj.value}`;
            }
        default:
            return obj.value;
    }
}

function getObjById(id, array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i].id == id) {
            return array[i];
        }
    }
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}
