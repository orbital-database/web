var CACHE = new Set();

var LANG;
fetch('base.json').then(resp => resp.text()).then(data => {
    LANG = JSON.parse(data);
});

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
    
    initExpandables();
    openMenu();
});

function loadJson(id) {
    var MISSIONS;
    for (let item of CACHE) {
        if (id == Object.keys(item)[0]) {
            MISSIONS = item;
            loadData(MISSIONS, id);
            break;
        }
    }
    
    if (!MISSIONS) {
        fetch('data/missions/'+id+'.json').then(resp => resp.text()).then(data => {
            MISSIONS = JSON.parse(data);
            loadData(MISSIONS, id);
            CACHE.add(MISSIONS);
        });
    }
}

function loadData(obj, id) {
    var path = document.getElementById("path");
    
    switch(id) {
    case 'offer_a':
        addMissions(obj.offer_a);
        path.innerHTML = `> ${LANG.offer} > ${LANG.r_a}`;
        break;
    case 'offer_b':
        addMissions(obj.offer_b);
        path.innerHTML = `> ${LANG.offer} > ${LANG.r_b}`;
        break;
    case 'offer_c':
        addMissions(obj.offer_c);
        path.innerHTML = `> ${LANG.offer} > ${LANG.r_c}`;
        break;
    case 'offer_d':
        addMissions(obj.offer_d);
        path.innerHTML = `> ${LANG.offer} > ${LANG.r_d}`;
        break;
    case 'offer_e':
        addMissions(obj.offer_e);
        path.innerHTML = `> ${LANG.offer} > ${LANG.r_e}`;
        break;
    case 'free_a':
        addMissions(obj.free_a);
        path.innerHTML = `> ${LANG.free} > ${LANG.r_a}`;
        break;
    case 'free_b':
        addMissions(obj.free_b);
        path.innerHTML = `> ${LANG.free} > ${LANG.r_b}`;
        break;
    case 'free_c':
        addMissions(obj.free_c);
        path.innerHTML = `> ${LANG.free} > ${LANG.r_c}`;
        break;
    case 'free_d':
        addMissions(obj.free_d);
        path.innerHTML = `> ${LANG.free} > ${LANG.r_d}`;
        break;
    case 'free_e':
        addMissions(obj.free_e);
        path.innerHTML = `> ${LANG.free} > ${LANG.r_e}`;
        break;
    case 'co_op':
        addMissions(obj.co_op);
        path.innerHTML = `> ${LANG.coop}`;
        break;
    }
    
    var instances = M.AutoInit();
    initExpandables();
}

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openRank(obj) {
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    var rank_text = document.querySelectorAll(".rank-text");
    for (let i = 0; i < rank_text.length; i++) {
        rank_text[i].classList.add("white-text");
        rank_text[i].classList.remove("white");
    }
    
    if (obj.id == "co_op") {
        obj.classList.remove("white-text");
        obj.setAttribute("style", "background-color: #fff !important;");
    } else {
        var coop = document.getElementById("co_op");
        coop.setAttribute("style", "background-color: #fff;");
        coop.classList.add("white-text");

        obj.classList.remove("white-text");
        obj.classList.add("white");
    }
    
    loadJson(obj.id);
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}

function addMissions(json) {
    var table = document.getElementById("list");
    
    for (let i = 0; i < json.length; i++) {
        var card = document.createElement("div");
        card.className = "card box-wrapper dark";
        
        var card_content1 = document.createElement("div");
        card_content1.className = "card-content transparent white-text";
        card.appendChild(card_content1);
        
        var card_title = document.createElement("span");
        card_title.className = "card-title";
        card_content1.appendChild(card_title);
        
        var card_data = document.createElement("p");
        card_content1.appendChild(card_data);
        
        // Check before creating tabs container
        if (json[i].enemies.length != 0 || 
            json[i].allies.length != 0 ||
            json[i].partners.partners.length != 0 || 
            json[i].encounters.rate != 0.0 ||
            json[i].bosses.length != 0) {
                var card_tabs = document.createElement("div");
                card_tabs.className = "card-tabs white-text";
                card.appendChild(card_tabs);
                
                var card_ul = document.createElement("ul");
                card_ul.className = "tabs tabs-fixed-width transparent white-text";
                card_tabs.appendChild(card_ul);
                
                var card_content2 = document.createElement("div");
                card_content2.className = "card-content card-content2 black white-text";
                card.appendChild(card_content2);
            }
        
        if (json[i].enemies.length != 0) {
            var card_li1 = document.createElement("li");
            card_li1.className = "tab";
            card_ul.appendChild(card_li1);
            
            var li1_a = document.createElement("a");
            li1_a.className = "active white-text";
            li1_a.href = `#enemies${i}`;
            li1_a.innerHTML = LANG.enemya;
            card_li1.appendChild(li1_a);
            
            card_content2.appendChild(enemyColapsable(json[i].enemies, i));
        }
        
        if (json[i].allies.length != 0) {
            var card_li3 = document.createElement("li");
            card_li3.className = "tab";
            card_ul.appendChild(card_li3);
            
            var li3_a = document.createElement("a");
            li3_a.className = "active white-text";
            li3_a.href = `#allies${i}`;
            li3_a.innerHTML = LANG.allya;
            card_li3.appendChild(li3_a);
            
            card_content2.appendChild(allyColapsable(json[i].allies, i));
        }

        if (json[i].encounters.rate != 0.0) {
            var card_li2 = document.createElement("li");
            card_li2.className = "tab";
            card_ul.appendChild(card_li2);
            
            var li2_a = document.createElement("a");
            li2_a.className = "white-text";
            li2_a.href = `#encounters${i}`;
            li2_a.innerHTML = `${LANG.inv} (${json[i].encounters.rate}%)`;
            card_li2.appendChild(li2_a);
            
            card_content2.appendChild(encounterColapsable(json[i].encounters.arsenals, i));
        }
        
        if (json[i].partners.partners.length != 0) {
            var card_li4 = document.createElement("li");
            card_li4.className = "tab";
            card_ul.appendChild(card_li4);
            
            var li4_a = document.createElement("a");
            li4_a.className = "active white-text";
            li4_a.href = `#partners${i}`;
            li4_a.innerHTML = `${LANG.freep} (${LANG.max} ${json[i].partners.max})`;
            card_li4.appendChild(li4_a);
            
            card_content2.appendChild(partnerColapsable(json[i].partners.partners, i));
        }
        
        if (json[i].bosses.length != 0) {
            var card_li5 = document.createElement("li");
            card_li5.className = "tab";
            card_ul.appendChild(card_li5);
            
            var li5_a = document.createElement("a");
            li5_a.className = "active white-text";
            li5_a.href = `#bosses${i}`;
            li5_a.innerHTML = LANG.boss;
            card_li5.appendChild(li5_a);
            
            card_content2.appendChild(bossesColapsable(json[i].bosses, i));
        }
        
        var goals = "";
        for (let j = 0; j < json[i].goals.length; j++) {
            goals = goals+`<br>- ${json[i].goals[j].text} (${json[i].goals[j].reward}c)`;
        }
        
        var fails = "";
        for (let j = 0; j < json[i].failures.length; j++) {
            fails = fails+`<br>- ${json[i].failures[j].text}`;
        }

        card_title.innerHTML = `<b>${json[i].title}</b>`;
        var line0 = json[i].subtitle;
        var line1 = `<b>${LANG.client}: </b>${json[i].client}`;
        var line2 = `<b>${LANG.area}: </b>${json[i].area}`;
        var line3 = `<b>${LANG.summ}: </b>${json[i].summary}`;
        
        var line4 = `<b>${LANG.goal}: </b> ${goals}`;
        var line5 = fails == "" ? "" : `<b>${LANG.fail}: </b> ${fails}`;
        
        card_data.innerHTML = `${line0}<hr>${line1}<br>${line2}<br>${line3}<br><hr>${line4}<br>${line5}`;
        
        table.appendChild(card);
    }
}

function getPartsData(arsenal) {
    var head = formatPart(LANG.head_l, arsenal.parts.head);
    var body = formatPart(LANG.body_l, arsenal.parts.body);
    var arm_r = formatPart(LANG.rarm_l, arsenal.parts.arm_r);
    var arm_l = formatPart(LANG.larm_l, arsenal.parts.arm_l);
    var legs = formatPart(LANG.leg_l, arsenal.parts.legs);
    
    var weapon_r = formatPart(LANG.rwep, arsenal.weapons.weapon_r);
    var weapon_l = formatPart(LANG.lwep, arsenal.weapons.weapon_l);
    var shoulder = formatPart(LANG.swep, arsenal.weapons.shoulder);
    var support = formatPart(LANG.aux_f, arsenal.weapons.support);
    var pylon_r = formatPart(LANG.rpyl, arsenal.weapons.pylon_r);
    var pylon_l = formatPart(LANG.lpyl, arsenal.weapons.pylon_l);
    
    var armor = `${head}<br>${body}<br>${arm_r}<br>${arm_l}<br>${legs}`;
    var weapons = `${weapon_r}<br>${weapon_l}<br>${shoulder}<br>${support}<br>${pylon_r}<br>${pylon_l}`;
    
    var res = `${armor}<br><hr>${weapons}`;
    
    return res;
}

function formatPart(text, part) {
    return `<b>${text}</b>: ${part.name} (${part.droprate}%)`;
}

function enemyColapsable(ajson, index){
    var arsenals_div = document.createElement("div");
    arsenals_div.id = `enemies${index}`;
    
    var arsenals_col = document.createElement("ul");
    arsenals_col.className = "collapsible expandable";
    
    for (let j = 0; j < ajson.length; j++) {
        var arsenal_li = document.createElement("li");
        arsenal_li.className = "active";
        
        var arsenal_header = document.createElement("div");
        arsenal_header.className = "collapsible-header";
        var name = ajson[j].outer == "None" ? ajson[j].name : `${ajson[j].outer} - ${ajson[j].name}`;
        arsenal_header.innerHTML = `<b>${name}</b>`;
        arsenal_li.appendChild(arsenal_header);
        
        var arsenal_content = document.createElement("div");
        arsenal_content.className = "collapsible-body";
        arsenal_li.appendChild(arsenal_content);
        
        var arsenal_equip = document.createElement("span");
        arsenal_equip.innerHTML = getPartsData(ajson[j]);
        arsenal_content.appendChild(arsenal_equip);
        
        arsenals_col.appendChild(arsenal_li);
    }
    
    arsenals_div.appendChild(arsenals_col);
    
    return arsenals_div;
}

function allyColapsable(ajson, index){
    var arsenals_div = document.createElement("div");
    arsenals_div.id = `allies${index}`;
    
    var arsenals_col = document.createElement("ul");
    arsenals_col.className = "collapsible expandable";
    
    for (let j = 0; j < ajson.length; j++) {
        var arsenal_li = document.createElement("li");
        arsenal_li.className = "active";
        
        var arsenal_header = document.createElement("div");
        arsenal_header.className = "collapsible-header collapsible-header2";
        var name = ajson[j].outer == "None" ? ajson[j].name : `${ajson[j].outer} - ${ajson[j].name}`;
        arsenal_header.innerHTML = `<b>${name}</b>`;
        arsenal_li.appendChild(arsenal_header);
        
        arsenals_col.appendChild(arsenal_li);
    }
    
    arsenals_div.appendChild(arsenals_col);
    
    return arsenals_div;
}

function partnerColapsable(ajson, index){
    var arsenals_div = document.createElement("div");
    arsenals_div.id = `partners${index}`;
    
    var arsenals_col = document.createElement("ul");
    arsenals_col.className = "collapsible expandable";
    
    for (let j = 0; j < ajson.length; j++) {
        var arsenal_li = document.createElement("li");
        arsenal_li.className = "active";
        
        var arsenal_header = document.createElement("div");
        arsenal_header.className = "collapsible-header collapsible-header2";
        var name = ajson[j].outer == "None" ? ajson[j].name : `${ajson[j].outer} - ${ajson[j].name} (${ajson[j].rate}%)`;
        arsenal_header.innerHTML = `<b>${name}</b>`;
        arsenal_li.appendChild(arsenal_header);
        
        arsenals_col.appendChild(arsenal_li);
    }
    
    arsenals_div.appendChild(arsenals_col);
    
    return arsenals_div;
}

function encounterColapsable(ajson, index){
    var arsenals_div = document.createElement("div");
    arsenals_div.id = `encounters${index}`;
    
    var arsenals_col = document.createElement("ul");
    arsenals_col.className = "collapsible expandable";
    
    for (let j = 0; j < ajson.length; j++) {
        var arsenal_li = document.createElement("li");
        arsenal_li.className = "active";
        
        var arsenal_header = document.createElement("div");
        arsenal_header.className = "collapsible-header";
        arsenal_header.innerHTML = `<b>${ajson[j].outer} - ${ajson[j].name} (${ajson[j].rate}%)</b>`;
        arsenal_li.appendChild(arsenal_header);
        
        var arsenal_content = document.createElement("div");
        arsenal_content.className = "collapsible-body";
        arsenal_li.appendChild(arsenal_content);
        
        var arsenal_equip = document.createElement("span");
        arsenal_equip.innerHTML = getPartsData(ajson[j]);
        arsenal_content.appendChild(arsenal_equip);
        
        arsenals_col.appendChild(arsenal_li);
    }
    
    arsenals_div.appendChild(arsenals_col);
    
    return arsenals_div;
}

function bossesColapsable(bjson, index){
    var bosses_div = document.createElement("div");
    bosses_div.id = `bosses${index}`;
    
    var bosses_col = document.createElement("ul");
    bosses_col.className = "collapsible expandable";
    
    for (let x = 0; x < bjson.length; x++) {
        var boss_li = document.createElement("li");
        boss_li.className = "active";
        
        var boss_header = document.createElement("div");
        boss_header.className = "collapsible-header";
        boss_header.innerHTML = `<b>${bjson[x].name}</b>`;
        boss_li.appendChild(boss_header);
        
        var boss_content = document.createElement("div");
        boss_content.className = "collapsible-body";
        boss_li.appendChild(boss_content);
        
        var drops = document.createElement("li");
        drops.className = "collection-item white-text";
        var dtext = "";
        
        if (bjson[x].base_drops.length != 0) {
            for (let i = 0; i < bjson[x].base_drops.length; i++) {
                var bdrops = "";
                for (let j = 0; j < bjson[x].base_drops[i].attachments.length; j++) {
                    bdrops = bdrops+`- ${bjson[x].base_drops[i].attachments[j].name} (${bjson[x].base_drops[i].attachments[j].rate}%)<br>`;
                }
                
                dtext = dtext+`${bjson[x].base_drops[i].text}:<br>${bdrops}<hr>`;
            }
        }

        if (bjson[x].weakpoint_drops.length != 0) {
            for (let i = 0; i < bjson[x].weakpoint_drops.length; i++) {
                var wdrops = "";
                for (let j = 0; j < bjson[x].weakpoint_drops[i].attachments.length; j++) {
                    wdrops = wdrops+`- ${bjson[x].weakpoint_drops[i].attachments[j].name} (${bjson[x].weakpoint_drops[i].attachments[j].rate}%)<br>`;
                }
                
                dtext = dtext+`${bjson[x].weakpoint_drops[i].text} (${bjson[x].weakpoint_drops[i].count}):<br>${wdrops}<hr>`;
            }
        }
        
        if (bjson[x].unique_drops.length != 0) {
            for (let i = 0; i < bjson[x].unique_drops.length; i++) {
                var udrops = "";
                for (let j = 0; j < bjson[x].unique_drops[i].drops.attachments.length; j++) {
                    udrops = udrops+`- ${bjson[x].unique_drops[i].drops.attachments[j].name} (${bjson[x].unique_drops[i].drops.attachments[j].rate}%)<br>`;
                }

                dtext = dtext+`${bjson[x].unique_drops[i].drops.text} (${bjson[x].unique_drops[i].value}):<br>${udrops}<hr>`;
            }
        }
        
        if (bjson[x].blueprints.length != 0) {
            var itm1 = document.createElement("li");
            itm1.className = "collection-item white-text";
            var bluep = "";
            for (let i = 0; i < bjson[x].blueprints.length; i++) {
                bluep = bluep+`- ${bjson[x].blueprints[i].name} (${bjson[x].blueprints[i].rate}%)<br>`;
            }
            bluep = `<b>${LANG.bluep}:</b><br>${bluep}`;
        }
        
        dtext = dtext+bluep;
        
        drops.innerHTML = dtext.slice(0, -4);
        boss_content.appendChild(drops);
        
        bosses_col.appendChild(boss_li);
    }
    
    bosses_div.appendChild(bosses_col);
    
    return bosses_div;
}
