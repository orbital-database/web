var LANG;
fetch('base.json').then(resp => resp.text()).then(data => {
    LANG = JSON.parse(data);
});

var RECLAIMERS;
fetch('data/reclaimers.json').then(resp => resp.text()).then(data => {
    RECLAIMERS = JSON.parse(data);
    loadGroupList();
    initExpandables();
    openMenu();
})

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
});

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openGroup(key) {
    obj = document.getElementById(key);
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    var group_text = document.querySelectorAll(".group-text");
    for (let i = 0; i < group_text.length; i++) {
        group_text[i].classList.add("white-text");
        group_text[i].classList.remove("white");
    }
    
    obj.classList.remove("white-text");
    obj.classList.add("white");

    var path = document.getElementById("path");
    addMembers(RECLAIMERS[key].members);
    path.innerHTML = `> ${RECLAIMERS[key].name.toUpperCase()}`;
    
    var instances = M.AutoInit();
    initExpandables();
}

function addMembers(json) {
    var table = document.getElementById("list");
    
    for (let i = 0; i < json.length; i++) {
        var col_ul = document.createElement("ul");
        col_ul.className = "collection";
        
        var col_itm_li = document.createElement("li");
        col_itm_li.className = "collection-item white-text";
        col_ul.appendChild(col_itm_li);
        
        var div_img = document.createElement("div");
        div_img.className = "imagebox";
        
        var col_img = document.createElement("img");
        col_img.className = "image";
        col_img.src = `../img/reclaimers/${json[i].id}.png`;
        div_img.appendChild(col_img);
        
        var col_title = document.createElement("span");
        col_title.className = "title";
        col_title.innerHTML = `<b>${json[i].outer.name}</b>`;
        div_img.appendChild(col_title);
        
        var col_line = document.createElement("p");
        col_line.className = "line";
        
        var skilllist = listSkills(json[i].outer.skills);
        var skilltext = skilllist ? `<hr><b> ${LANG.sskil}:</b><br>${skilllist}` : "";
        
        col_line.innerHTML = `<hr>${json[i].outer.description}${skilltext}`;
        div_img.appendChild(col_line);
        
        var col_line2 = document.createElement("p");
        col_line2.className = "line";
        col_line2.innerHTML = `<hr><b>${LANG.ars}</b>: ${json[i].arsenal.name}<br>${json[i].arsenal.description}`;
        div_img.appendChild(col_line2);
        
        col_itm_li.appendChild(div_img);
        
        var col_line3 = document.createElement("p");
        col_line3.className = "line";
        col_line3.innerHTML = `<hr><b>${LANG.miss_u}:</b>`;
        col_itm_li.appendChild(col_line3);
        
        col_itm_li.appendChild(addMissions(json[i].missions));
            
        table.appendChild(col_ul);
    }
}

function addMissions(missions) {
    var missions_col = document.createElement("ul");
    missions_col.className = "collapsible expandable";
    
    if (missions.enemy.length) {
        var enemy_li = document.createElement("li");
        
        var enemy_header = document.createElement("div");
        enemy_header.className = "collapsible-header";
        enemy_header.innerHTML = `<b>${LANG.aenemy}:</b>`;
        enemy_li.appendChild(enemy_header);
        
        var enemy_content = document.createElement("div");
        enemy_content.className = "collapsible-body";
        
        for (let i = 0; i < missions.enemy.length; i++) {        
            var text = `${missions.enemy[i].title}<br>
                        ${missions.enemy[i].subtitle} (${missions.enemy[i].category.toUpperCase().replace("_", " ")})
                        ${i == missions.enemy.length-1 ? "" : "<br><hr>"}`

            enemy_content.innerHTML = enemy_content.innerHTML+text;
        }
        
        enemy_li.appendChild(enemy_content);
        missions_col.appendChild(enemy_li);
    }
    
    if (missions.ally.length) {
        var ally_li = document.createElement("li");
        
        var ally_header = document.createElement("div");
        ally_header.className = "collapsible-header";
        ally_header.innerHTML = `<b>${LANG.aally}:</b>`;
        ally_li.appendChild(ally_header);
        
        var ally_content = document.createElement("div");
        ally_content.className = "collapsible-body";
        
        for (let i = 0; i < missions.ally.length; i++) {        
            var text = `${missions.ally[i].title}<br>
                        ${missions.ally[i].subtitle} (${missions.ally[i].category.toUpperCase().replace("_", " ")})
                        ${i == missions.ally.length-1 ? "" : "<br><hr>"}`

            ally_content.innerHTML = ally_content.innerHTML+text;
        }
        
        ally_li.appendChild(ally_content);
        missions_col.appendChild(ally_li);
    }
    

    if (missions.encounter.length) {
        var enc_li = document.createElement("li");
        
        var enc_header = document.createElement("div");
        enc_header.className = "collapsible-header";
        enc_header.innerHTML = `<b>${LANG.ainv}:</b>`;
        enc_li.appendChild(enc_header);
        
        var enc_content = document.createElement("div");
        enc_content.className = "collapsible-body";
        
        for (let i = 0; i < missions.encounter.length; i++) {
            var text = `${missions.encounter[i].title}<br>
                        ${missions.encounter[i].subtitle} (${missions.encounter[i].category.toUpperCase().replace("_", " ")})
                        <br>${LANG.mencr}: ${missions.encounter[i].mission_rate}%
                        <br>${LANG.arser}: ${missions.encounter[i].arsenal_rate}%
                        ${i == missions.encounter.length-1 ? "" : "<br><hr>"}`

            enc_content.innerHTML = enc_content.innerHTML+text;
        }
        
        enc_li.appendChild(enc_content);
        missions_col.appendChild(enc_li);
    }
    
    if (missions.partner.length) {
        var part_li = document.createElement("li");
        
        var part_header = document.createElement("div");
        part_header.className = "collapsible-header";
        part_header.innerHTML = `<b>${LANG.afreep}:</b>`;
        part_li.appendChild(part_header);
        
        var part_content = document.createElement("div");
        part_content.className = "collapsible-body";
        
        for (let i = 0; i < missions.partner.length; i++) {
            var text = `${missions.partner[i].title}<br>
                        ${missions.partner[i].subtitle} (${missions.partner[i].category.toUpperCase().replace("_", " ")})
                        <br>${LANG.arate}: ${missions.partner[i].rate}%
                        ${i == missions.partner.length-1 ? "" : "<br><hr>"}`

            part_content.innerHTML = part_content.innerHTML+text;
        }
        
        part_li.appendChild(part_content);
        missions_col.appendChild(part_li);
    }
    
    return missions_col;
}

function listSkills(array) {
    var res = "";
    for (let i = 0; i < array.length; i++) {
        var type = array[i].type;
        var value = array[i].value;
        var compl = value ? `- ${type} (${value}%)` : `- ${type}`
        res = res + `${compl}<br>`;
    }
    
    return res;
}

function loadGroupList() {
    var list = document.getElementById("group-list");
    
    Object.keys(RECLAIMERS).forEach(function(key,index) {
        var gli = document.createElement("li");
        gli.className = "group";
        
        var ndiv = document.createElement("div");
        ndiv.className = "group-text white-text";
        ndiv.id = key;
        ndiv.innerHTML = RECLAIMERS[key].name;
        
        ndiv.addEventListener("click", function(e) {
            openGroup(key);
        }, false);
        
        gli.appendChild(ndiv);
        
        list.appendChild(gli);
    });
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}
