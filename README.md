This is the website with the processed data, feel free to make a merge request if you want to contribute (or hit me up in discord Hario#0540).

---

Tools used for datamining DxM:
- [hactool][hactool]
- [quickbms][quickbms]
- [UE Viewer/umodel][umodel]
- [locres2csv][locres2csv]
- [UAssetParser][uaparser]
- [JohnWickParse][jwparse] (same functionality as above, has issues with duplicated keys)

---

Thanks to:
- Japanese translation: \@dxm_akitsu ([@DXM_akitsu][DXM_akitsu])
- Japanese translation and additional data: \@berylskid ([@U46_cast][beryl])
- Help fixing the missing drops: @Radu131

[hactool]: https://github.com/SciresM/hactool/
[quickbms]: https://aluigi.altervista.org/quickbms.htm
[umodel]: https://www.gildor.org/en/projects/umodel
[locres2csv]: https://drive.google.com/file/d/1-ftM3xAukoogkU5SmNKmsXYLA4b6omgJ/view
[jwparse]: https://github.com/SirWaddles/JohnWickParse/
[uaparser]: https://github.com/healingbrew/UAssetParser
[DXM_akitsu]: https://twitter.com/DXM_akitsu
[beryl]: https://twitter.com/U46_cast
